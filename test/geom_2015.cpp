#include "geom_2015.hpp"
#include <iostream>

///##########POSITION##############

position::position(){
  val[0] = val[1] = val[2] = 0.0;
}

position::position(double x, double y, double z){
  val[0] = x;
  val[1] = y;
  val[2] = z;
}

position::position(double pos[3]){
  val[0] = pos[0];
  val[1] = pos[1];
  val[2] = pos[2];
}

position::position(vector v){
  val[0] = v.x();
  val[1] = v.y();
  val[2] = v.z();
}

double position::x(){
  return val[0];
}

double position::y(){
  return val[1];
}

double position::z(){
  return val[2];
}

void position::set_x(double v){
  val[0] = v;
}

void position::set_y(double v){
  val[1] = v;  
}

void position::set_z(double v){
  val[2] = v;
}

position position::operator+(position pos){
  return position(pos.x() + val[0], pos.y() + val[1], pos.z() + val[2]);
}

position position::operator+(vector v){
  return position(v.x() + val[0], v.y() + val[1], v.z() + val[2]);
}

position position::operator-(vector v){
  return position(val[0] - v.x(), val[1] - v.y(), val[2] - v.z());
}

vector position::operator-(position pos){
  return vector(val[0] - pos.x(), val[1] - pos.y(), val[2] - pos.z());
}

position position::operator*(double sclr){
  return position(sclr * val[0], sclr * val[1], sclr * val[2]); 
}



//####################VECTOR###################

vector::vector(){
  val[0] = val[1] = val[2] = 0;
}

vector::vector(double x, double y, double z){
  val[0] = x;
  val[1] = y;
  val[2] = z;
}

vector::vector(double v[]){
  val[0] = v[0];
  val[1] = v[1];
  val[2] = v[2];
}


double vector::x(){
  return val[0];
}

double vector::y(){
  return val[1];
}

double vector::z(){
  return val[2];
}

void vector::set_x(double v){
  val[0] = v;
}

void vector::set_y(double v){
  val[1] = v;  
}

void vector::set_z(double v){
  val[2] = v;  
}

double vector::length(){
  return sqrt(val[0] * val[0] + val[1] * val[1] + val[2] * val[2]);
}

vector vector::operator*(double k){
  return vector(val[0] * k, val[1] * k, val[2] * k);
}

vector vector::operator+(vector v){
  return vector(val[0] + v.x(), val[1] + v.y(), val[2] + v.z());
}

vector vector::operator-(vector v){
  return vector(val[0] - v.x(), val[1] - v.y(), val[2] - v.z());
}

vector vector::operator/(double k){
  return vector(val[0] / k, val[1] / k, val[2] / k);
}

position vector::operator+(position pnt){
  return position(val[0] + pnt.x(), val[1] + pnt.y(), val[2] + pnt.z());
}

double vector::operator%(vector vec){
  return val[0] * vec.x() + val[1] * vec.y() + val[2] * vec.z();
}

vector& vector::operator=(position pstn){
  val[0] = pstn.x();
  val[1] = pstn.y();
  val[2] = pstn.z();

  return *this;
}


unit_vector::unit_vector():vector(1.0, 0, 0){}

unit_vector::unit_vector(double x, double y, double z){
  double norm = sqrt(x*x + y*y + z*z);
  if(norm == 0){
    //vector(1.0, 0, 0);
    set_x(1.0);
    set_y(0);
    set_z(0);
    return;
  }
  //vector(x / norm, y / norm, z / norm);
  set_x(x / norm);
  set_y(y / norm);
  set_z(z / norm);  
}

unit_vector::unit_vector(vector vec){
  if(vec.length() == 0){
    //    vector(1.0, 0, 0);
    set_x(1.0);
    set_y(0);
    set_z(0);
    return;
  }
  //vector(vec / vec.length());
  set_x(vec.x() / vec.length());
  set_y(vec.y() / vec.length());
  set_z(vec.z() / vec.length());    
}



//#################VECTOR EXTENDED###################
Vector::Vector(Vector &v){
  m_size = v.size();
  m_val = new double[m_size];
  for(int i = 0; i < m_size; ++i){
    m_val[i] = v[i];
  }
}

Vector::~Vector(){
  delete[] m_val;
}

double Vector::length(){
  return sqrt(length2());
}

double Vector::length2(){
  double norm = 0;
  for(int i = 0; i < m_size; ++i){
    norm += m_val[i] * m_val[i];
  }
  return norm;
}

void Vector::set(int i, double val){
  if((i >= 0) && (i < m_size)){
    m_val[i] = val;
  }
}

void Vector::set(double val[]){
  for(int i = 0; i < m_size; ++i){
    m_val[i] = val[i];
  }
}

Vector Vector::operator+(Vector & v){
  Vector tv = v;
  for(int i = 0; i < m_size; ++i){
    tv.set(i, v[i] + m_val[i]);
  }
  return tv;
}

Vector Vector::operator-(Vector & v){
  Vector tv = v;
  for(int i = 0; i < m_size; ++i){
    tv.set(i, m_val[i] - v[i]);
  }  
  return tv;  
}

Vector& Vector::operator=(Vector & v){
  //m_size = v.size();
  if(m_size == v.size()){
    for(int i = 0; i < m_size; ++i){
      m_val[i] = v[i];
    }
    return *this;
  }else{
    m_size = v.size();
    delete[] m_val;
    m_val = new double[m_size];
    for(int i = 0; i < m_size; ++i){
      m_val[i] = v[i];
    }
    return *this;    
  }
}

double Vector::operator[](int ioe){
  if((ioe >= 0) && (ioe <= m_size))
    return m_val[ioe];
  else
    return 0.0;
}

void Vector::print(){


  
  std::cout << "[ ";

  if(m_size == 0){
    std::cout << "NULL ]t" << std::endl;
    return;
  }
  
  for(int i = 0; i < (m_size - 1); ++i){
    std::cout.width(7);
    std::cout.precision(5);
    std::cout << m_val[i] << ", ";
  }
  std::cout.width(7);
  std::cout.precision(5);
  std::cout << m_val[m_size - 1] << " ]t" << std::endl;
}

void Vector::clear(){
  for(int i = 0; i < m_size; ++i){
    m_val[i] = 0;
  }
}



//#############################MATRIX##############################
//ROW:行、COLUMN:列
Matrix::Matrix(Matrix &m){
  m_sz_row = m.size_row();
  m_sz_col = m.size_col();

  m_elm = new double[m_sz_row * m_sz_col];
  for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
    *(m_elm + i) = m.element(0, i);
  }
}


Matrix::~Matrix(){
  delete[] m_elm;
}

double Matrix::element(int row, int col) const{
  return *(m_elm + m_sz_col * row + col);
}

Matrix Matrix::operator*(Matrix &m){
  Matrix tm(m_sz_row, m.size_col());
  double tmp;
  if(m_sz_col != m.size_row()){
    tm.eye();
    return tm;
  }

  for(int i = 0; i < m_sz_row; ++i){
    for(int j = 0; j < m.size_col(); ++j){
      tmp = 0;
      for(int k = 0; k < m_sz_col; ++k){
	tmp += *(m_elm + m_sz_col * i + k) * m.element(k, j);
      }
      tm.set(i, j, tmp);
    }
  }

  return tm;
}

Vector Matrix::operator*(Vector &vec){
  Vector tv(m_sz_row);
  double tmp;
  if(m_sz_col != vec.size()){
    tv.clear();
    return tv;
  }
  
  for(int i = 0; i < m_sz_row; ++i){
    tmp = 0;
    for(int j = 0; j < m_sz_col; ++j){
      tmp += *(m_elm + m_sz_col * i + j) * vec[j];
    }
    tv.set(i, tmp);
  }
  return tv;
}

Matrix Matrix::operator*(double val){
  Matrix tm(m_sz_row, m_sz_col);
  for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
    tm.set(0, i, m_elm[i] * val);
  }
  return tm;
}

Matrix Matrix::operator+(Matrix &m){
  Matrix tm(m_sz_row, m_sz_col);
 
  if((m_sz_col != m.size_col()) || (m_sz_row != m.size_row())){
    tm.eye();
    return tm;
  }
  
  for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
    tm.set(0, i, m_elm[i] + m.element(0, i));
  }
  return tm;  
}

Matrix Matrix::operator-(Matrix &m){
  Matrix tm(m_sz_row, m_sz_col);
 
  if((m_sz_col != m.size_col()) || (m_sz_row != m.size_row())){
    tm.eye();
    return tm;
  }
  
  for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
    tm.set(0, i, m_elm[i] - m.element(0, i));
  }
  return tm;  
}

Matrix& Matrix::operator=(Matrix &m){

  if((m_sz_col != m.size_col()) || (m_sz_row != m.size_row())){
    delete[] m_elm;
    m_sz_col = m.size_col();
    m_sz_row = m.size_row();
    m_elm = new double[m_sz_col * m_sz_row];
  }
  
  for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
    m_elm[i] = m.element(0, i);
  }  
  return *this;
}

// Matrix& Matrix::operator=(Matrix &&m){
//   if((m_sz_col != m.size_col()) || (m_sz_row != m.size_row())){
//     delete[] m_elm;
//     m_sz_col = m.size_col();
//     m_sz_row = m.size_row();
//     m_elm = new double[m_sz_col * m_sz_row];
//   }
  
//   for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
//     m_elm[i] = m.element(0, i);
//   }  
//   return *this;
// }

Matrix Matrix::operator-(){
  for(int i = 0; i < (m_sz_col * m_sz_row); ++i){
    m_elm[i] *= -1.0;
  }  
  return *this;
}

Matrix Matrix::transpose(){
  // int i, j;
  // double *tm = new double[m_sz_row * m_sz_col];
  // for(i = 0; i < m_sz_row; ++i){
  //   for(j = 0; j < m_sz_col; ++j){
  //     *(tm + m_sz_col * i + j) = *(m_elm + m_sz_col * i + j);
  //     *(m_elm + m_sz_row * j + i) = *(tm + m_sz_col * i + j);
  //   }
  // }

  Matrix tm(m_sz_col, m_sz_row);

  for(int i = 0; i < m_sz_row; ++i){
    for(int j = 0; j < m_sz_col; ++j){
      tm.set(j, i, *(m_elm + m_sz_col * i + j));
    }
  }
  return tm;
}

void Matrix::clear(){
  for(int i = 0; i < (m_sz_row * m_sz_col); ++i){
    *(m_elm + i) = 0;
  }
}

void Matrix::eye(){
  int mi = (m_sz_row < m_sz_col) ? m_sz_row : m_sz_col;
  clear();
  for(int i = 0; i < mi; ++i){
    *(m_elm + m_sz_col * i + i) = 1.0;
  }
}

void Matrix::print(){
  if((m_sz_col == 0) && (m_sz_row == 0)){
    std::cout << "[ NULL ]" << std::endl;
    return;
  }

  std::cout << m_sz_row << "x" << m_sz_col << std::endl;
  
  for(int i = 0; i < m_sz_row; ++i){
    for(int j = 0; j < (m_sz_col); ++j){
      std::cout.width(7);
      std::cout.precision(5);	  
      std::cout <<  *(m_elm + m_sz_col * i + j) << " ";
    }
    //std::cout << " " << *(m_elm + m_sz_col * i + (m_sz_row - 1)) << std::endl;
    std::cout << "\n";
  }

  std::cout << std::endl;
}


/////#############################################FRIENDS###################################
vector operator*(double k, vector vec){
  return vec * k;
  //  return vector(vec.x() * k, vec.y() * k, vec.z() * k);
}

vector operator*(vector vec1, vector vec2){
  return vector(
		vec1.y() * vec2.z() - vec1.z() * vec2.y(),
		vec1.z() * vec2.x() - vec1.x() * vec2.z(),
		vec1.x() * vec2.y() - vec1.y() * vec2.x()
		);
}

class Matrix2: public Matrix{
public:
  Matrix2():Matrix(){}
  Matrix2(int sz_row,int sz_col):Matrix(sz_row, sz_col){}
  Matrix2(Matrix &m):Matrix(m){}		// Copy Constructor
  
  //  int	size_row()const{ return size_row(); }
  //  int	size_col()const{ return size_col(); }
 Matrix2& operator *= (const double sc){
    for(int i = 0; i < (size_col() * size_row()); ++i){
      set(0, i, element(0, i) * sc);
    }  
    return *this;
  }

 Matrix2& operator /= (const double sc){
    operator *=(1.0 / sc);
    return *this;
  }

 Matrix2& operator += (const double sc){
    for(int i = 0; i < (size_col() * size_row()); ++i){
      set(0, i, element(0, i) + sc);
    }  
    return *this;    
  }

 Matrix2& operator -= (const double sc){
    operator+=(-1.0 * sc);
    return *this;
  }

  //#TODO Avoid code duplication
 Matrix2& operator += (const Matrix& m){
    for(int i = 0; i < (size_col() * size_row()); ++i){
      set(0, i, element(0, i) + m.element(0, i));
    }  
    return *this;    
  }

 Matrix2& operator -= (const Matrix& m){
    for(int i = 0; i < (size_col() * size_row()); ++i){
      set(0, i, element(0, i) - m.element(0, i));
    }  
    return *this;       
  }

  Matrix2& operator += (const Matrix2& m){
    for(int i = 0; i < (size_col() * size_row()); ++i){
      set(0, i, element(0, i) + m.element(0, i));
    }  
    return *this;    
  }
  
  Matrix2& operator -= (const Matrix2& m){
    for(int i = 0; i < (size_col() * size_row()); ++i){
      set(0, i, element(0, i) - m.element(0, i));
    }  
    return *this;       
  }

  
};

int SolveSimuaneousEquationDirect(Matrix& mx, Vector& y, Vector& a){
  int i, j;

  if(mx.size_row() < mx.size_col()){
    a.clear();
    return -1;
  }

  for(i = 0; i < mx.size_col(); ++i){
    double crowm = 1;
    for(j = 0; j < mx.size_row(); ++j){
      crowm *= mx.element(j, i);
    }
    if(crowm == 0){
      a.clear();
      return -2;
    }
  }

  Matrix2 *vrow = new Matrix2[mx.size_row()];
  
  bool is_pivotted = false;
  
  //pivotter table
  int *pt = new int[mx.size_row()];
  for(i = 0; i < mx.size_row(); ++i){
    pt[i] = i;
  }

  //row vector initialization
  for(i = 0; i < mx.size_row(); ++i){
    Matrix2 vtmp(1, mx.size_col() + 1);
    for(j = 0; j < mx.size_col(); ++j){
      vtmp.set(0, j, mx.element(i, j));
    }
    vtmp.set(0, mx.size_col(), y[i]); 
    
    vrow[i] = vtmp;
    // std::cout << "#1" << i << std::endl;
    // vrow[i].print();
  }

  

  //MAIN
  for(i = 0; i < mx.size_col(); ++i){
    //pivot
    is_pivotted = false;
    for(j = i; j < mx.size_row(); ++j){
      if(vrow[pt[j]].element(0, i) != 0){
	pt[i] = pt[j];
	pt[j] = i;
	is_pivotted = true;
	break;
      }
    }

    // for(int k = 0; k < mx.size_row(); ++k){
    //   std::cout << "A" << i << k << std::endl;
    //   vrow[pt[k]].print();
    // }

    if(!is_pivotted){
      delete[] vrow;
      delete[] pt;
      a.clear();
      return -3;
    }
    
    //    std::cout << "#2" << i << ":" << vrow[pt[i]].element(0, i) << std::endl;    
    vrow[pt[i]] /= vrow[pt[i]].element(0, i); // make fu row vector
    //    vrow[pt[i]].print();
    
    for(j = 0; j < mx.size_row(); ++j){
      if(j != i){
	vrow[pt[j]] -= (vrow[pt[i]] * vrow[pt[j]].element(0, i));
	//(vrow[pt[i]] * 3.0); これはスーパークラスの演算子が呼ばれMatrix型が返される
	//そのためMatrix2でMatrixを受け取れるのをオーバーロードできないとコンパイルエラー
	//rValueが入力されるがconst Refなので参照可
      }
    }
    
    // for(int k = 0; k < mx.size_row(); ++k){
    //   std::cout << "B" << i << k << std::endl;
    //   vrow[pt[k]].print();
    // }
  }

  //output
  for(i = 0; i < mx.size_row(); ++i){
    a.set(i, vrow[i].element(0, mx.size_col()));
  }

  delete[] vrow;
  delete[] pt;
  return 1;
}

int Matrix::SSED(Vector &y, Vector &a){
  return SolveSimuaneousEquationDirect(*this, y, a);
}






 








