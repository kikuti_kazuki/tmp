#include "geom_2015.hpp"
#include <cstdio>

void print(position p){
  printf("P[%5.5f,\t%5.5f,\t%5.5f]\n", p.x(), p.y(), p.z());
}

void print(vector p){
  printf("V[%5.5f,\t%5.5f,\t%5.5f]\n", p.x(), p.y(), p.z());
}

void print(unit_vector p){
  printf("UV[%5.5f,\t%5.5f,\t%5.5f]\n", p.x(), p.y(), p.z());
}

void setm(Matrix& m, double v[]){
  for(int i = 0; i < (m.size_row() * m.size_col()); ++i){
    m.set(0, i, v[i]);
  }
}

void setm2(Matrix& m, double start, double step){
  for(int i = 0; i < (m.size_row() * m.size_col()); ++i){
    m.set(0, i, start + (double)i * step);
  } 
}

int main(){

  //############### POSITION ##############
  {
    printf("\nPOSITION\n\n");
    
    position a;
    position b(1,2,3);
    vector v(3,4,5);
    position c(v);

    print(a);print(b);print(c);print(v);

    print(b + c);
    print(b + v);
    print(b - v);
    print(b - c);
    print(b * 2.0);
    
  }
  
  //################ VECTOR ###############
  {
  printf("\nVECTOR\n\n");

  double tv[] = {3, 4, 5};
  
  vector a;
  vector b(1, 2, 3);
  vector c(tv);
  position d(c);

  print(a);print(b);print(c);
  printf("LENGTH: %5.5f\n", a.length());
  printf("LENGTH: %5.5f\n", b.length());
  printf("LENGTH: %5.5f\n", c.length());

  print(b * 2.0);
  print(b + c);
  print(b - c);
  print(b / 2.0);
  print(b + d);

  printf("INNER PRODUCT: %5.5f\n", b %c);

  print(2.0 * b);
  print(b * c);
  print(vector(1.0, 0,0) * vector(0, 1, 0));
  print((b = c));

  printf("\nUNIT VECTOR\n\n");
  unit_vector e;
  unit_vector f(2, 3, 4);
  unit_vector g(b);

  print(e);print(f);print(g);
  printf("LENGTH: %5.5f\n", e.length());
  printf("LENGTH: %5.5f\n", f.length());
  printf("LENGTH: %5.5f\n", g.length());
  }

  //########## EXTENDED VECTOR ##########

  {
  printf("\nEXTENDED VECTOR\n\n");

  double tv[] = {1.1, 2.111111, 3.34564, 4.99, 5};
  double tv2[] = {3, 4, 5, 6, 7};
  
  Vector a;
  Vector b(4);
  Vector c(5, tv);
  Vector d(c);

  a.print();
  b.print();
  c.print();
  d.print();

  printf("LENGTH: %5.5f\n", a.length());
  printf("LENGTH: %5.5f\n", b.length());
  printf("LENGTH: %5.5f\n", c.length());
  printf("LENGTH: %5.5f\n", d.length());

  printf("LENGTH2: %5.5f\n", a.length2());
  printf("LENGTH2: %5.5f\n", b.length2());
  printf("LENGTH2: %5.5f\n", c.length2());
  printf("LENGTH2: %5.5f\n", d.length2());

  printf("SIZE: %d\n", a.size());
  printf("SIZE: %d\n", b.size());
  printf("SIZE: %d\n", c.size());
  printf("SIZE: %d\n", d.size());
  
  d.set(tv2);
  c.print();
  d.print();
  (c + d).print();
  (c - d).print();
  (c = d).print();
  c.clear(), c.print();
  
  }

  
  //########### EXTENDED MATRIX ###########
  {
  printf("\nEXTENDED MATRIX\n\n");

  Matrix a;
  Matrix b(3, 4);
  Matrix c(b);

  printf("p\n");
  a.print();
  b.print();
  c.print();

  double tv[] = {1,2,3,4, 5,6,7,8, 9,10,11,12};
  setm(b, tv);
  b.print();
  b.transpose().print();
  (-b).print();
  c.eye();
  c.print();
  c.transpose().print();
  c.clear();
  c.print();

  Matrix d(1,3), e(3, 2), du(1,1),
    A(20, 30), B(30, 80), C(5, 4), D(5, 4), E(5, 5);
  Vector fv(3);
  double ttv[] = {1, 2, 3};
  fv.set(ttv);

  du.set(0, 0, 2.0);
  setm2(d, 1, 1);
  setm2(e, 4, 1);
  setm2(A, 0, 1.235);
  setm2(B, 200, 2.666);
  setm2(C, 2, 0.823);
  setm2(D, -5, 1);
  setm2(E, 3, -9);
  
  printf("#################################\n");
  fv.print();
  d.print();
  e.print();
  du.print();
  (e * 3.0).print();
  (d * fv).print();
  (d*e).print();

  //  A.print();
  //  B.print();
  //  (A * B).print();
  (d = e).print();

  printf("#################################\n");
  C.print();
  D.print();
  E.print();
  (C + C).print();
  (C + D).print();
  (C + E).print();

  printf("#################################\n");
  Matrix MX(2, 2);
  Vector Y(2);
  Vector OUT(2);
  OUT.clear();
  double mmx[] = {3,5, 2,3};
  double mmy[] = {2, -1};
  setm(MX, mmx);
  Y.set(mmy);
  MX.print();
  Y.print();
  OUT.print();
  
  //SolveSimultaneousEquationDirect(MX, Y, OUT);
  printf("SSED: %d\n", MX.SSED(Y, OUT));
  OUT.print();
  
  }

  {
    // Matrix A(2,2), B(2, 2), C;
    // A.eye();
    // B.eye();
    // C = A + B;
    // C.print();
}

  
  return 0;
}













