 /*************************************************************************
							Geometry Definition 
					Tokyo University of Science
							9/Jan/2004
			Matrix and Vector are revised.	9/Jan/2004
//			position2 added					16/Aug/2005
//			clear of Matrix	  added			4/Sep/2005
									by K.Kato
**************************************************************************/

#ifndef GEOM
#include "math.h"
#define M_PI	3.141592653589793238
#define NULL 0

class	vector;
class	Matrix;
class	Vector;

//int SolveSimultaneousEquationDirect(Matrix& mx,Vector& y,Vector& a);

class	position {
		double val[3];
    public:
		position();
		position(double,double,double);
		position(double[3]);
		position(vector);
		double	x(); 
		double	y(); 
		double	z();
		void	set_x(double v);
		void	set_y(double v);
		void	set_z(double v);
		position operator+(position);
		position operator+(vector);
		position operator-(vector);
		vector operator-(position);
		position operator*(double);		// this を使う

};
//
//
//
class	vector {
		double val[3];
    public:
		vector();
		vector(double x,double y,double z);
		vector(double v[]);
		double	x(); 
		double	y(); 
		double	z();
		void	set_x(double v);
		void	set_y(double v);
		void	set_z(double v);
		double	length(); 
		vector operator *(double k);
		vector operator +(vector);
		vector operator -(vector);
		vector operator /(double k);
		position operator +(position pnt);
		
		double	operator %(vector vec);	// 内積
		friend vector	operator *(double k,vector vec);		//　k倍のベクトル
		friend vector	operator *(vector vec1,vector vec2);		//　外積
		vector &operator =(position);
    // friend
		friend class position;
};
class	unit_vector : public vector {
    public:
		unit_vector();
		unit_vector(double x,double y,double z);
		unit_vector(vector vec);
};

//
//	Extended Vector & Matrix definition
//
class	Vector {
		int		m_size;
		double	*m_val;
    public:
		Vector() {m_size=0;m_val=NULL;}
		Vector(int size) { m_size = size; m_val = new double[size]; }
		Vector(int size,double val[]) { m_size = size; m_val = new double[size]; set(val); }
		Vector(Vector &v);			// Copy constructor
		~Vector();
		double	length();
		double	length2();			// 長さの二乗				
		double	element(int i) { return m_val[i]; }
		void	set(int i,double val);
		void	set(double val[]);
		int		size() { return m_size; }
		Vector operator +(Vector&);
		Vector operator -(Vector&);
		Vector &operator =(Vector&);
		double operator [](int);
		void print();
		void clear();
//	friend	int	SolveSimultaneousEquationDirect(Matrix& mx,Vector& y,Vector &a);
};
class	Matrix	{
  int		m_sz_row,m_sz_col;
  double	*m_elm;
public:
		Matrix() {m_sz_row=m_sz_col=0; m_elm=NULL; }
		Matrix(int sz_row,int sz_col) { m_sz_row=sz_row; m_sz_col=sz_col; m_elm=new double[m_sz_row*m_sz_col]; }
		Matrix(Matrix &m);		// Copy Constructor
		~Matrix();
		double	element(int row,int col) const;
		int	size_row() { return m_sz_row; }
		int	size_col() { return m_sz_col; }
		void set(int row,int col,double val) { *(m_elm+m_sz_col*row+col)=val; }
		Matrix	operator *(Matrix &m);
		Vector	operator *(Vector &vec);
		Matrix  operator *(double val);
		Matrix  operator +(Matrix &m);
		Matrix  operator -(Matrix &m);
                Matrix&	operator = (Matrix &m); //rvalue は渡せない
  //                Matrix& operator = (Matrix &&m);  
		Matrix operator -();
		Matrix transpose();
  //int invert();
		void clear();				// ゼロクリア
		void eye();				// 単位行列
		void print();				// 内容表示
                int SSED(Vector& y,Vector &a);
//	//
	friend	int	SolveSimultaneousEquationDirect(Matrix& mx,Vector& y,Vector &a);
};

//int SolveSimultaneousEquationDirect(Matrix& mx,Vector& y,Vector &a);


#define GEOM 1
#endif
