
#ifndef BPC_H
#define BPC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <serialib.h>

//UCPデータ構造のサイズ
//int8, uint8を用いたUCPのサイズ
#define SIZE_INT8_UCP 5
//floated ucpのエスケープされないサイズ
#define SIZE_FLOAT_UCP 9

const byte BPC_ESC = 0xFF;
const byte BPC_START = 0x01;
const byte BPC_STOP = 0x02;
const byte BPC_SINGLE = 0xA0;
const byte BPC_DOUBLE = 0xA1;

const byte BPC_SIGNED_UCP = 0xA2;
const byte BPC_UNSIGNED_UCP = 0xA3;
const byte BPC_FLOATED_UCP = 0xA4;
const byte BPC_INT8_UCP = 0xA5;
const byte BPC_INT16_UCP = 0xA6;
const byte BPC_INT32_UCP = 0xA7;
const byte BPC_INT64_UCP = 0xA8;
const byte BPC_UINT8_UCP = 0xA9;
const byte BPC_UINT16_UCP = 0xB0;
const byte BPC_UINT32_UCP = 0xB1;
const byte BPC_UINT64_UCP = 0xB2;


//UCPデータ構造のサイズ
//int8, uint8を用いたUCPのサイズ
const byte SIZE_INT8_UCP = 5;
//floated ucpのエスケープされないサイズ
const byte SIZE_FLOAT_UCP = 9;

//解釈器関係
//読み込み失敗
const byte FAILED_TO_READ = 123;
//バッファ空
const byte BUFFER_EMPTY = 1;
//読み込み完了
const byte READ_COMPLETE = 2;

//バイナリパケットの最大サイズ
//un/signed UCPの時
const byte SIZE_INT8_UCP_BP_MAX = 14;
//floated UCPの時
const byte SIZE_FLOAT_UCP_BP_MAX = 22;

class BPC{
 public:

  serialib LS;

  BPC();
  ~BPC();

  void mk_signedUCP(byte dst[], byte type, 
		    short src1, short src);

  void mk_unsignedUCP(byte dst[], byte type, 
		      unsigned shot src1, unsigned short src2);

  void mk_floatedUCP(byte dst[], byte type, 
		   float src1, float src2);

  void send_BPC_singedUCP(byte ucp_type, 
			       short src1, short src2);

  void send_BPC_floatedUCP(byte ucp_type, 
			   float src1, float src2);




}

#endif
