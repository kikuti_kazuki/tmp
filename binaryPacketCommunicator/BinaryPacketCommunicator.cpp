#include "BinaryPacketCommunicator.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>


BinaryPacketCommunicator::BinaryPacketCommunicator(){
  sl.Open("/dev/ttyS0", 115200);
}

BinaryPacketCommunicator::BinaryPacketCommunicator(const char * path2port){
  sl.Open(path2port, 115200);
}

BinaryPacketCommunicator::BinaryPacketCommunicator(const char * path2port,
						   const int& baudrate){
  sl.Open(path2port, baudrate);
}

BinaryPacketCommunicator::~BinaryPacketCommunicator(){
  sl.Close();
}



//それぞれの型に応じたBPCシグネチャを返す
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(int8_t t){
  return BPCdataset::BPC_INT8_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(int16_t t){
  return BPCdataset::BPC_INT16_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(int32_t t){
  return BPCdataset::BPC_INT32_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(int64_t t){
  return BPCdataset::BPC_INT64_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(uint8_t t){
  return BPCdataset::BPC_UINT8_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(uint16_t t){
  return BPCdataset::BPC_UINT16_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(uint32_t t){
  return BPCdataset::BPC_UINT32_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(uint64_t t){
  return BPCdataset::BPC_UINT64_UCP;
}

uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(float t){
  return BPCdataset::BPC_FLOATED_UCP;
}
uint8_t BinaryPacketCommunicator::get_ucp_bpc_type(double t){
  return BPCdataset::BPC_FLOATED_UCP;
}

template <class T>
int32_t BinaryPacketCommunicator::get_unescaped_ucp_size(const T &t){
  return (sizeof(t) * 2 + sizeof(uint8_t));
}

template <class T>
int32_t BinaryPacketCommunicator::get_fullescaped_ucp_bpc_size(const T &t){
  return (
	  (sizeof(t) * 2 + sizeof(uint8_t)) * 2
	  + sizeof(uint8_t) * 5
	  );
}

template <class T>
uint8_t BinaryPacketCommunicator::mk_ucpdata(uint8_t *dst, const uint8_t& type,
					     const T& src1, const T& src2){
  T tmp[2];
  dst[0] = type;
  tmp[0] = src1, tmp[1] = src2;
  memcpy((void *)&dst[1], (void *)tmp, sizeof(T) * 2);

  return 0;
}

template <class T>
uint8_t BinaryPacketCommunicator::send_packet(const uint8_t& ucp_type,
					      const T& src1, const T& src2){
  int ucp_count, buff_count;

  //BPCパケット用バッファ
  uint8_t * buff_bpc = malloc(get_fullescaped_ucp_bpc_size((T)0));
  //UCPパケット用バッファ
  uint8_t * buff_ucp = malloc(get_unescaped_ucp_size((T)0));

  //START byte
  //制御バイトなのでエスケープする
  buff_bpc[0] = BPCdataset::BPC_ESC;
  buff_bpc[1] = BPCdataset::BPC_START;

  //payload type
  buff_bpc[2] = get_ucp_bpc_type((T)0);

  //UCPを構成
  mk_ucpdata(buff_ucp, ucp_type, src1, src2);

  //UCPをエスケープしコピーしていく
  //データそのものがエスケープ文字であった時、それをエスケープする
  //つまりESCを送るときはESC ESCとなる
  //ここまでインデックス2まで埋めたので次は３
  buff_count = 3;
  for (ucp_count = 0; ucp_count < get_unescaped_ucp_size((T)0); ucp_count++)
    {
      if (buff_ucp[ucp_count] == BPCdataset::BPC_ESC)
	{
	  buff_bpc[buff_count++] = BPCdataset::BPC_ESC;
	  buff_bpc[buff_count++] = BPCdataset::BPC_ESC;
	}
      else
	{
	  buff_bpc[buff_count++] = buff_ucp[ucp_count];
	}
    }

  //ESC STOPを挿入
  buff_bpc[buff_count++] = BPCdataset::BPC_ESC;
  buff_bpc[buff_count++] = BPCdataset::BPC_STOP;

  sl.Write((const void *)buff_bpc, buff_count);
  
  free(buff_bpc);
  free(buff_ucp);

  return 0;
}



















