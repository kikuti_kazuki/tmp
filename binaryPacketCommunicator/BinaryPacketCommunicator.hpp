#ifndef _BINARYPACKETCOMMUNICATOR_H_
#define _BINARYPACKETCOMMUNICATOR_H_

#include "serialib.h"
#include <stdint.h>

class BPCdataset{
public:

  
  static const uint8_t BPC_ESC = 0xFF;
  static const uint8_t BPC_START = 0x01;
  static const uint8_t BPC_STOP = 0x02;
  static const uint8_t BPC_SINGLE = 0xA0;
  static const uint8_t BPC_DOUBLE = 0xA1;

  static const uint8_t BPC_SIGNED_UCP = 0xA2;
  static const uint8_t BPC_UNSIGNED_UCP = 0xA3;
  static const uint8_t BPC_FLOATED_UCP = 0xA4;
  
  static const uint8_t BPC_INT8_UCP = 0xA5;
  static const uint8_t BPC_INT16_UCP = 0xA6;
  static const uint8_t BPC_INT32_UCP = 0xA7;
  static const uint8_t BPC_INT64_UCP = 0xA8;
  static const uint8_t BPC_UINT8_UCP = 0xA9;
  static const uint8_t BPC_UINT16_UCP = 0xB0;
  static const uint8_t BPC_UINT32_UCP = 0xB1;
  static const uint8_t BPC_UINT64_UCP = 0xB2;


  //UCPデータ構造のサイズ
  //int8, uint8を用いたUCPのサイズ
  static const uint8_t SIZE_INT8_UCP = 5;
  //floated ucpのエスケープされないサイズ
  static const uint8_t SIZE_FLOAT_UCP = 9;

  //解釈器関係
  //読み込み失敗
  static const uint8_t FAILED_TO_READ = 123;
  //バッファ空
  static const uint8_t BUFFER_EMPTY = 1;
  //読み込み完了
  static const uint8_t READ_COMPLETE = 2;

  //バイナリパケットの最大サイズ
  //un/signed UCPの時
  static const uint8_t SIZE_INT8_UCP_BP_MAX = 14;
  //floated UCPの時
  static const uint8_t SIZE_FLOAT_UCP_BP_MAX = 22;
  
private:
  
};



class BinaryPacketCommunicator{
public:

  BinaryPacketCommunicator();
  BinaryPacketCommunicator(const char * path2port);
  BinaryPacketCommunicator(const char * path2port, const int& baudrate);
  ~BinaryPacketCommunicator();

  template <class T> uint8_t send_packet(const uint8_t& ucp_type,
					     const T& src1, const T& src2);
private:
  
  //型にあったBCPシグネチャを返す
  uint8_t get_ucp_bpc_type(int8_t t);
  uint8_t get_ucp_bpc_type(int16_t t);
  uint8_t get_ucp_bpc_type(int32_t t);
  uint8_t get_ucp_bpc_type(int64_t t);
  uint8_t get_ucp_bpc_type(uint8_t t);
  uint8_t get_ucp_bpc_type(uint16_t t);
  uint8_t get_ucp_bpc_type(uint32_t t);
  uint8_t get_ucp_bpc_type(uint64_t t);
  uint8_t get_ucp_bpc_type(float t);
  uint8_t get_ucp_bpc_type(double t);

  template <class T> int32_t get_unescaped_ucp_size(const T& t);
  template <class T> int32_t get_fullescaped_ucp_bpc_size(const T& t);

  template <class T> uint8_t mk_ucpdata(uint8_t * dst, const uint8_t& type,
					const T& src1, const T& src2);

  serialib sl;
  
};


#endif /* _BINARYPACKETCOMMUNICATOR_H_ */














