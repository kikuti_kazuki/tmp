#include "BPC.h"

#define DEV_PORT "/dev/ttyS0"

//Constructor
BPC::BPC()
{
LS.Open(DEV_PORT, 115200);
}

//Destructor
BPC::~BPC()
{
LS.Close();
}

void BPC::mk_signedUCP(byte dst[], byte type, 
		       short src1, short src2){
short tmp[2];
dst[0] = type;
tmp[0] = src1, tmp[1] = src2;
memcpy((void *)&dst[1], (void *)tmp, sizeof(short)*2);
}



void BPC::mk_unsignedUCP(byte dst[], byte type, 
		    unsigned shot src1, unsigned short src2){
  unsigned short tmp[2];
  dst[0] = type;
  tmp[0] = src1, tmp[1] = src2;
  memcpy((void *)&dst[1], (void *)tmp, sizeof(unsigned short)*2);  
}

void BPC::mk_floatedUCP(byte dst[], byte type, 
			float src1, float src2){
  float tmp[2];
  dst[0] = type;
  tmp[0] = src1, tmp[1] = src2;
  memcpy((void *)&dst[1], (void *)tmp, sizeof(float)*2);
}

void BPC::send_BPC_singedUCP(byte ucp_type, 
			     short src1, short src2){
  int ucp_count, buff_count;

  //最大14 byteなので14バイト確保する
  byte buff[SIZE_INT8_UCP_BP_MAX];

  //UCP
  //エスケープされないUCPパケットは5byte
  byte ucp_buff[SIZE_INT8_UCP];

  //START byte
  //制御バイトなのでエスケープする
  buff[0] = BPC_ESC;
  buff[1] = BPC_START;

  //payload type
  buff[2] = BPC_SIGNED_UCP;

  //UCPを構成
  mk_signedUCP(ucp_buff, ucp_type, src1, src2);

  //UCPをエスケープしコピーしていく
  //データそのものがエスケープ文字であった時、それをエスケープする
  //つまりESCを送るときはESC ESCとなる
  //ここまでインデックス2まで埋めたので次は３
  buff_count = 3;
  for (ucp_count = 0; ucp_count < SIZE_INT8_UCP; ucp_count++)
    {
      if (ucp_buff[ucp_count] == BPC_ESC)
	{
	  buff[buff_count++] = BPC_ESC;
	  buff[buff_count++] = BPC_ESC;
	}
      else
	{
	  buff[buff_count++] = ucp_buff[ucp_count];
	}
    }
  
  //ESC STOPを挿入
  buff[buff_count++] = BPC_ESC;
  buff[buff_count++] = BPC_STOP;
  //この時buff_countがパケット長になっている

  //送信
  //uart_send_nbyte((char *)buff, buff_count);
  // SP.Write(buff, 0, buff_count);
  //TODO: SENDING METHOD
  LS.Write((const void *)buff, buff_count);
}

void BPC::send_BPC_floatedUCP(byte ucp_type, 
			      float src1, float src2){
  int ucp_count, buff_count;

  //最大22 byteなので22バイト確保する
  byte buff[SIZE_FLOAT_UCP_BP_MAX];

  //UCP
  //エスケープされないUCPパケットは9byte
  byte ucp_buff[SIZE_FLOAT_UCP];
  
  //if (SP.IsOpen == false) {
  //  return;
  //}

  //START byte
  //制御バイトなのでエスケープする
  buff[0] = BPC_ESC;
  buff[1] = BPC_START;

  //payload type
  buff[2] = BPC_FLOATED_UCP;
  
  //UCPを構成
  mk_floatedUCP(ucp_buff, ucp_type, src1, src2);

  //UCPをエスケープしコピーしていく
  //データそのものがエスケープ文字であった時、それをエスケープする
  //つまりESCを送るときはESC ESCとなる
  //ここまでインデックス2まで埋めたので次は３
  buff_count = 3;
  for (ucp_count = 0; ucp_count < SIZE_FLOAT_UCP; ucp_count++)
    {
      if (ucp_buff[ucp_count] == BPC_ESC)
	{
	  buff[buff_count++] = BPC_ESC;
	  buff[buff_count++] = BPC_ESC;
	}
      else
	{
	  buff[buff_count++] = ucp_buff[ucp_count];
	}
    }
  
  //ESC STOPを挿入
  buff[buff_count++] = BPC_ESC;
  buff[buff_count++] = BPC_STOP;
  //この時buff_countがパケット長になっている
  
  //#TODO SENDING
  //送信
  //uart_send_nbyte((char *)buff, buff_count);
  // SP.Write(buff, 0, buff_count);
  LS.Write((const void *)buff, buff_count);
}

