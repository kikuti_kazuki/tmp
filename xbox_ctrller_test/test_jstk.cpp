#include "joystick_xbox360.hpp"
#include <stdio.h>
#include <unistd.h>




int main(){

  JoystickXbox360 j360;
  j360.initialize();

  while(1){
    j360.update();

    printf("AXLX:%d\tAXLY:%d\tAXRX:%d\tAXRY:%d\tTRGL:%d\tTRGR:%d\n",
	   j360.AxisLX, j360.AxisLY, j360.AxisRX, j360.AxisRY, j360.TrigerL, j360.TrigerR);

    if(j360.bXbox == true){
      return 0;
    }
    
    usleep(20000);
  }
  
  return 0;
}
