#include "ugv_communicator.hpp"

template <class Tl, class Ts>
UGVdataset<Tl, Ts>::UGVdataset()
  :wheelpower(0),
   angle_steering(0),
   angle_camera_Yaw(0),
   angle_camera_Pitch(0),
   mode(0)
{
  ;
}


template <class Tl, class Ts>
uint8_t UGVdataset<Tl, Ts>::operator==(const UGVdataset & tls){
  uint8_t out = 0;

  out |= (wheelpower == tls.wheelpower) 
    ? UGVdataset::DIFF_WHEELPOWER : 0;
  out |= (angle_steering == tls.angle_steering) 
    ? UGVdataset::DIFF_ANGLE_STEERING : 0;
  out |= (angle_camera_Yaw == tls.angle_camera_Yaw) 
    ? UGVdataset::DIFF_ANGLE_CAMERA_YAW : 0;
  out |= (angle_camera_Pitch == tls.angle_camera_Pitch) 
    ? UGVdataset::DIFF_ANGLE_CAMERA_PITCH : 0;
  out |= (mode == tls.mode) ? UGVdataset::DIFF_MODE : 0;

  return out;
}

template <class Tl, class Ts>
UGVdataset<Tl, Ts> & UGVdataset<Tl, Ts>::operator=(const UGVdataset &src){
  wheelpower = src.wheelpower;
  angle_steering = src.angle_steering;
  angle_camera_Yaw = src.angle_camera_Yaw;
  angle_camera_Pitch = src.angle_camera_Pitch;
  mode = src.mode;
  return *this;
}

template <class Tl, class Ts>
UGVcommunicator<Tl, Ts>::UGVcommunicator()
  :buff_pkt(NULL),
   sock(0),
   type_communicator(UGVcommunicator<Tl, Ts>::TYPE_COMM_UNDEFINED)
{
  buff_pkt = new uint8_t(sizeof(Tl) * 4 + sizeof(Ts));

  size_packet_max = sizeof(Tl) * 4 + sizeof(Ts);
  ;
}

template <class Tl, class Ts>
UGVcommunicator<Tl, Ts>::~UGVcommunicator(){
  delete[] buff_pkt;
  ;
}

template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::initialize_sender(const char *ipaddr,
						   const int &port){

  if(type_communicator != TYPE_COMM_UNDEFINED){
    return 0xFF;
  }else{
    type_communicator = TYPE_COMM_SEND;
  }
  
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = inet_addr(ipaddr);

  return 0;
}

template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::initialize_sender(){


  if(type_communicator != TYPE_COMM_UNDEFINED){
    return 0xFF;
  }else{
    type_communicator = TYPE_COMM_SEND;
  }
  
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(12346);
  addr.sin_addr.s_addr = inet_addr("192.168.9.2");


  
  return 0;
  
}

template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::deinitialize_sender(){
  if(sock != 0){
    close(sock);
  }

  type_communicator = TYPE_COMM_UNDEFINED;
  
  return 0;
}

template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::send(const Tl wheelpower,
				      const Tl angle_steering,
				      const Tl amgle_camera_Yaw,
				      const Tl angle_camera_Pitch,
				      const Ts mode){
  int32_t outsize;

  update_status(wheelpower, angle_steering, amgle_camera_Yaw, angle_camera_Pitch, mode);
  mk_packet(&outsize, &data_press);
  if((data_prev == data_press) == 0x00){
    return 0;
  }else{
    return sendto(sock, buff_pkt, outsize, 0, (sockaddr *)&addr, sizeof(struct sockaddr));
  }
  return 0;
}


//一つ前のステータスを退避、新たにアップデート
template <class Tl, class Ts>
void UGVcommunicator<Tl, Ts>::update_status(const Tl wheelpower,
					    const Tl angle_steering, 
					    const Tl angle_camera_Yaw,
					    const Tl angle_camera_Pitch,
					    const Ts mode){

  UGVdataset<Tl, Ts> 
  
  data_prev = data_press;
  data_press.wheelpower = wheelpower;
  data_press.angle_steering = angle_steering;
  data_press.angle_camera_Yaw = angle_camera_Yaw;
  data_press.angle_camera_Yaw = angle_camera_Pitch;
  data_press.mode = mode;

  
  return;
}




template <class Tl, class Ts>
void UGVcommunicator<Tl, Ts>::mk_packet(int32_t &outsize,
					const UGVdataset<Tl, Ts> &data){
  //タイプシグネチャなしでやってみる
  outsize = sizeof(Tl) * 4 + sizeof(Ts);
 
  memcpy((void *)&buff_pkt[0], &(data.wheelpower), sizeof(Tl));
  memcpy((void *)&buff_pkt[sizeof(Tl)], &(data.angle_steering), sizeof(Tl));
  memcpy((void *)&buff_pkt[sizeof(Tl) * 2], &(data.angle_camera_Pitch), sizeof(Tl));
  memcpy((void *)&buff_pkt[sizeof(Tl) * 3], &(data.angle_camera_Yaw), sizeof(Tl));
  memcpy((void *)&buff_pkt[sizeof(Tl) * 4], &(data.mode), sizeof(Ts));
}


//レシーバ関連
template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::initialize_reciever(const int &port,
						     const char * path2port,
						     const int& baudrate){

  
  if(type_communicator != TYPE_COMM_UNDEFINED){
    return 0xFF;
  }else{
    type_communicator = TYPE_COMM_RECIEVE;
  }

  bpc.initialize(path2port, baudrate);
  
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = INADDR_ANY;

  bind(sock, (struct sockaddr *)&addr, sizeof(addr));

  return 0;
}

template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::initialize_reciever(){

  if(type_communicator != TYPE_COMM_UNDEFINED){
    return 0xFF;
  }else{
    type_communicator = TYPE_COMM_RECIEVE;
  }

  bpc.initialize("/dev/ttyO1", BPCdataset::BPC_BAUDRATE_115200);
  
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(12346);
  addr.sin_addr.s_addr = INADDR_ANY;
  
  bind(sock, (struct sockaddr *)&addr, sizeof(addr));
  
  return 0;
  
}

template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::deinitialize_reciever(){
  if(sock != 0){
    close(sock);
  }

  type_communicator = TYPE_COMM_UNDEFINED;
  
  return 0;
}



//レシーバ
template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::recieve(){
  UGVdataset<Tl, Ts> tmp_ud;
  uint8_t *buff = new uint8_t(this->size_packet_max);
  int re = -1;
  
  re = recv(this->sock, buff, this->size_packet_max, 0);

  if (re == -1){
    printf("%s:%s:Failed to recieve data\n", __FUNCTION__, __LINE__);
    perror(":");
    return -1;
  }else{
    printf("%d bytes recieved\n", re);
  }
  
  memcpy(&(tmp_ud.wheelpower), &buff[0], sizeof(Tl));
  memcpy(&(tmp_ud.angle_steering), &buff[sizeof(Tl)], sizeof(Tl));
  memcpy(&(tmp_ud.angle_camera_Pitch), &buff[sizeof(Tl) * 2], sizeof(Tl));
  memcpy(&(tmp_ud.angle_camera_Yaw), &buff[sizeof(Tl) * 3], sizeof(Tl));
  memcpy(&(tmp_ud.mode), &buff[sizeof(Tl) * 4], sizeof(Ts));


  pthread_mutex_lock(&(this->th_mtx));

  //ミューテックスをロックしてからデータをコピー
  data_out = tmp_ud;

  //ここでシリアルポートに流してもいいかも
  //#TODO

  //とりあえずパワーとステアリング角度のみ出力
  bpc.send_packet<Tl>(UGVdataset<int, int>::UCP_TYPE_WP_AW,
		      data_out.wheelpower, data_out.angle_steering);
  
  pthread_mutex_unlock(&(this->th_mutex));
  
  delete[] buff;

  return re;
}



//レシーバループ
template<class Tl, class Ts>
void* UGVcommunicator<Tl, Ts>::loop_reciever(void *p){
  printf("run reciever\n");
  while(1){
    reinterpret_cast<UGVcommunicator<Tl, Ts>*>(p)->recieve();
  }
}


//レシーバスレッドの起動
template<class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::reciever_launch(){
  printf("Launch reciever thread\n");

  pthread_mutex_init(&(th_mtx), NULL);
  pthread_create(&th_hdl, NULL, &UGVcommunicator<Tl, Ts>::loop_reciever, this);

  return 0;
  
}

//スレッドセーフ？な出力
template <class Tl, class Ts>
uint8_t UGVcommunicator<Tl, Ts>::reciever_getstatus(Tl &wp, Tl &as,
						    Tl &acy, Tl &acp,
						    Ts &md){
  pthread_mutex_lock(&th_mtx);
  
  wp = data_out.wheelpower;
  as = data_out.angle_steering;
  acy = data_out.angle_camera_Yaw;
  acp = data_out.angle_camera_Pitch;
  md = data_out.mode;
  
  pthread_mutex_unlock(&th_mtx);
}





















