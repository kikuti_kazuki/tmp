#include "joystick_xbox360.hpp"



class UGVcontroller{
public:
  UGVcontroller();
  UGVcontroller(const double wheelbase);
  UGVcontroller(const double wheelbase, const double steering_limit_DEG);
  ~UGVcontroller();

  double steering_angle_limit_DEG;
  double steering_angle_limit_RAD;
  
private:
  //ジョイスティック
  JoystickXbox360 jstk;

  //後輪パワー
  double wheel_power;
  //ステアリング角度
  double steering_angle;

  //ステアリング角度RAW　ジョイスティックそのまま
  double steering_angle_raw;

  //カメラ角度
  double cam_angle_pitch;
  double cam_angle_yaw;

  //車体ベクトル [0]:X=>throttle
  double body_vector[3];

  //車体レート [0]:Roll, [1]:Pitch, [2]:Yaw
  double body_rate[3];

  //車体ホイールベース UNIT:mm
  double body_wheelbase;


  //車体状態生成
  uint8_t mk_bodystate(JoystickXbox360& joy);

  //ステアリング角生成
  uint8_t mk_steering_angle(bool is_yawrate_constant);

  //送信パケット生成
  
  
};










