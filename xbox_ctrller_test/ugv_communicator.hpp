#ifndef _UGV_COMMUNICATOR_H_
#define _UGV_COMMUNICATOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h> 
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <asm/types.h>
#include <pthread.h>

#include "BinaryPacketCommunicator.hpp"


template <class Tl, class Ts>
class UGVdataset{
public:

  UGVdataset();
  ~UGVdataset();
  
  static const uint8_t TYPE_WHEELPOWER     = 0xA1;
  static const uint8_t TYPE_ANGLE_STEERING = 0xA2;
  static const uint8_t TYPE_ANGLE_CAMERA   = 0xA3;
  static const uint8_t TYPE_MODE           = 0xA4;  
  static const uint8_t TYPE_ALL            = 0xA5;
  
  static const uint8_t DIFF_WHEELPOWER           = 0b00000001;
  static const uint8_t DIFF_ANGLE_STEERING       = 0b00000010;
  static const uint8_t DIFF_ANGLE_CAMERA_YAW     = 0b00000100;
  static const uint8_t DIFF_ANGLE_CAMERA_PITCH   = 0b00001000;
  static const uint8_t DIFF_MODE                 = 0b00010000; 
  static const uint8_t DIFF_MATHC                = 0x00;


  //UCP TYPE
  static const uint8_t UCP_TYPE_WHEELPOWER     = 0xD0;
  static const uint8_t UCP_TYPE_ANGLE_STEERING = 0xD2;
  static const uint8_t UCP_TYPE_ANGLE_CAMERA   = 0xD3;
  static const uint8_t UCP_TYPE_MODE           = 0xD4;
  static const uint8_t UCP_TYPE_WP_AW          = 0xD5;
  
  Tl wheelpower;
  Tl angle_steering;
  Tl angle_camera_Yaw;
  Tl angle_camera_Pitch;
  Ts mode;
  
  //比較用
  uint8_t operator==(const UGVdataset & tls);
  
  //コピー用
  UGVdataset & operator=(const UGVdataset &src);
  

private:
};


template<class Tl, class Ts>
class UGVcommunicator{
public:

  UGVcommunicator();
  ~UGVcommunicator();

  //connect UGV
  uint8_t initialize_sender();
  uint8_t initialize_sender(const char * ipaddr, const int & port);
  uint8_t deinitialize_sender();

  uint8_t initialize_reciever();
  uint8_t initialize_reciever(const int & port,
			      const char * path2port, const int& baudrate);
  uint8_t deinitialize_reciever();

  
  //Build a packet
  /*
    パケット構成
    必要なもの

    　パケットサイズ
    　データ識別
    
  */
  
  
  /*
    今回送るもの
    
    後輪パワー
    ステアリング角
    カメラ角度

    モータードライバの有効／無効
    

    こんなもんか…
    UDPだけでええやろ

    ストリーマの制御はストリーマ側で独自にコミュニケータを持つようにするのでいらない。

  */

  /*
    後輪パワー　　INT32　
    ステアリング角 INT32　
    カメラ角度　　INT32 mt by 2　
    MDED　　　　 INT8　
  */

  /*
    パケットの構成
    サイズは可変
    +  +  +  +  + +                                                               
     WP AS AP AY M 

     T: Type
     WP: wheel power
     AS: angle of steering
     AP: angle of camera's pitch
     AY: angle of camera's yaw
     M: mode
     
                                                                        
   */





  uint8_t send(const Tl wheelpower, const Tl angle_steering, 
	       const Tl amgle_camera_Yaw, const Tl angle_camera_Pitch,
	       const Ts mode);

  //レシーバ向け
  /*
    レシーバは別スレッドで動かすことにする。
    別の読み込み関数を通すことでミューテックスクリア
   */
  //レシーバスレッドの起動
  uint8_t reciever_launch();
  //レシーバより読み込み
  uint8_t reciever_getstatus(Tl& wp, Tl& as, Tl& acy, Tl& acp, Ts& md);
  
private:
  bool is_changed;
  uint8_t * buff_pkt;
  int sock;
  struct sockaddr_in addr;

  int size_packet_max;

  int type_communicator;

  static const int TYPE_COMM_UNDEFINED = 0;
  static const int TYPE_COMM_SEND = 1;
  static const int TYPE_COMM_RECIEVE = 2;

  
  //マルチスレッド関連
  pthread_t th_hdl;
  pthread_mutex_t th_mtx;

  //UGVとの通信　シリアルポート関連
  BinaryPacketCommunicator bpc;
  

  //現在のUGVデータと一ステップ前の入力データを保持する
  UGVdataset<Tl, Ts> data_prev;
  UGVdataset<Tl, Ts> data_press;

  //レシーバ用データ　変更のあるデータしか送られてこないので差分を取る必要なし
  UGVdataset<Tl, Ts> data_out;
  
  //データの退避、ステータスのアップデート
  void update_status(const Tl wheelpower, const Tl angle_steering, 
		     const Tl amgle_camera_Yaw, const Tl angle_camera_Pitch,
		     const Ts mode);

  //送信用パケットの生成
  void mk_packet(int32_t& outsize, const UGVdataset<Tl, Ts>& data);

  //レシーバ関数
  uint8_t recieve();

  //レシーバループ
  static void *loop_reciever(void *p);
  

};

#endif /* _UGV_COMMUNICATOR_H_ */
















