#include "ugv_control.hpp"

#define _USE_MATH_DEFINES
#include <math.h>

UGVcontroller::UGVcontroller(){
  //initailize joystick(for xbox360 controller)
  jstk.initialize();
  body_wheelbase = 300;
}

UGVcontroller::UGVcontroller(const double wheelbase){
  jstk.initialize();
  body_wheelbase = wheelbase;
}

UGVcontroller::UGVcontroller(const double wheelbase,
			     const double steering_limit_DEG){
  jstk.initialize();
  body_wheelbase = wheelbase;
  steering_angle_limit_DEG = steering_limit_DEG;
  steering_angle_limit_RAD = steering_limit_DEG * M_PI / 360.0;
}



UGVcontroller::~UGVcontroller(){

}


/*
  調整項目
  コントローラのAXISスカラ倍定数を実際どんなもんにするか

  追加
  カメラパン・チルト用のカメラベクトル出力,
  これはアディショナルとする


*/
uint8_t UGVcontroller::mk_bodystate(JoystickXbox360 &joy){
  // double L = body_wheelbase;
  // double R;

  //車体レート [0]:Roll, [1]:Pitch, [2]:Yaw
  //double body_rate[3];

  //車体ベクトル [0]:X=>throttle
  //double body_vector[3];
  
  //ジョイスティック状態更新
  joy.update();

  //車体レートのピッチと、車体ベクトルのXを決定する
  body_vector[0] = (double)(joy.TrigerR + JoystickXbox360::AXIS_OFFSET
		    - (joy.TrigerL + JoystickXbox360::AXIS_OFFSET)) * 0.01;

  body_rate[2] = (double)(joy.AxisLX) * 0.1;

  //コントローラ-ステアリング間に何もなしの角度
  steering_angle_raw = (double)(joy.AxisLX)
    * steering_angle_limit_RAD / JoystickXbox360::AXIS_MAX;
  
  
  return 1;
}



/*
  調整項目
  ヨーレートが実際どんなもんか？
  車体進行方向ベクトルが実際どんなもんか？
  
  ヨーレート制御に重きを置いているから、
  ヨーレートは現実世界の単位をそのまま使いたい
*/
uint8_t UGVcontroller::mk_steering_angle(bool is_yawrate_constant){

  if(is_yawrate_constant){
    //ヨーレート一定演算を行う
    steering_angle = 2.0 * atan((body_wheelbase * body_rate[2]) / body_vector[0]);
  }else{
    //ヨーレート一定を考えない
    steering_angle = steering_angle_raw;

  }
  
  //リミット処理
  steering_angle =
    (steering_angle > steering_angle_limit_RAD)
    ? steering_angle_limit_RAD : steering_angle;
  
  steering_angle =
    (steering_angle < (-1.0 * steering_angle_limit_RAD))
    ? (-1.0 * steering_angle_limit_RAD) : steering_angle;

  return 1;
}











