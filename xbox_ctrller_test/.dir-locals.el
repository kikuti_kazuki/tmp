;;;contents of .dir-locals.el
((nil . (
	 (eval . (progn
		   (defun my-project-specific-function ()
		     (setq ac-clang-cflags (
					    append ac-clang-cflags '("-I/usr/local/include/SDL2")
						   )
			   )
		                          ;;;tell ac-clang that we updated the cflags
		     (ac-clang-update-cmdlineargs)
		     )
		   (my-project-specific-function)
		   )
	       )
	 )
      ))
