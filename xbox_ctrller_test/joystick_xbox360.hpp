#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>


class xbox360{
 public:
  xbox360();
  ~xbox360();

  bool bA, bB, bX, bY;
  bool bRB, bLB;
  bool bXbox, bBACK, bSTART;
  bool bHatUp, bHatDown, bHatLeft, bHatRight;
  bool bAxisL, bAxisR;

  int AxisLX;
  int AxisLY;

  int AxisRX;
  int AxisRY;

  int TrigerL;
  int TrigerR;

  static const int AXIS_MAX    = 65535;
  static const int AXIS_OFFSET = 32768;

  
private:
  
};


class JoystickXbox360 : public xbox360{

public:
  JoystickXbox360();
  ~JoystickXbox360();

  uint8_t initialize();
  uint8_t deinitialize();

  uint8_t update();

private:
  SDL_Joystick *jstk;
};
















