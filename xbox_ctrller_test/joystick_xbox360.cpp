#include "joystick_xbox360.hpp"

xbox360::xbox360()
  :bA(false),
   bB(false),
   bX(false),
   bY(false),

   bRB(false),
   bLB(false),

   bHatUp(false),
   bHatDown(false),
   bHatLeft(false),
   bHatRight(false),

   bAxisL(false),
   bAxisR(false),

   AxisLX(0),
   AxisLY(0),

   AxisRX(0),
   AxisRY(0),

   TrigerL(0),
   TrigerR(0)
{
  ;
}

xbox360::~xbox360(){

}

JoystickXbox360::JoystickXbox360(){
  
}

JoystickXbox360::~JoystickXbox360(){

}


uint8_t JoystickXbox360::initialize(){
  const char *jstkname;

  SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);
  if(SDL_InitSubSystem(SDL_INIT_JOYSTICK) < 0){
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
		 "Could not initialize SDL: %s\n",
		 SDL_GetError());
    return 0;
  }

  if(SDL_NumJoysticks() == 0){
    SDL_Log("a Joystick is not attached\n");
    return 0;
  }else{
    jstkname = SDL_JoystickNameForIndex(0);
    SDL_Log("Joystick attached: %s", (jstkname) ? jstkname : "Unknown Joystick");
    jstk = SDL_JoystickOpen(0);
    if(jstk == NULL){
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
		   "SDL_JoystickOpen(%d) failed: %s\n",
		   0,
		   SDL_GetError()
		   );
      return 0;
    }
  }
  
  return 1;
}

uint8_t JoystickXbox360::deinitialize(){
  SDL_JoystickClose(jstk);
  SDL_QuitSubSystem(SDL_INIT_JOYSTICK);
  return 0;
}
   
uint8_t JoystickXbox360::update(){
  bool *buttons;
  Uint8 hat_pos;

  int i;
  
  buttons = (bool *)malloc(sizeof(bool) * SDL_JoystickNumButtons(jstk));

  SDL_JoystickUpdate();

  //Get buttons status
  for(i = 0; i < SDL_JoystickNumButtons(jstk); i++){
    if(SDL_JoystickGetButton(jstk, i) == SDL_PRESSED){
      buttons[i] = true;
    }else{
      buttons[i] = false;
    }
  }

  bA = buttons[0];
  bB = buttons[1];
  bX = buttons[2];
  bY = buttons[3];
  bLB = buttons[4];
  bRB = buttons[5];
  bBACK = buttons[6];
  bSTART = buttons[7];
  bXbox = buttons[8];
  bAxisL = buttons[9];
  bAxisR = buttons[10];

  //Get Hat positions
  hat_pos = SDL_JoystickGetHat(jstk, 0);
  bHatUp =    (hat_pos & SDL_HAT_UP) ? true : false;
  bHatDown =  (hat_pos & SDL_HAT_DOWN) ? true : false;
  bHatLeft =  (hat_pos & SDL_HAT_LEFT) ? true : false;
  bHatRight = (hat_pos & SDL_HAT_RIGHT) ? true : false;

  //Get Axes status
  AxisLX = (int)SDL_JoystickGetAxis(jstk, 0);
  AxisLY = (int)SDL_JoystickGetAxis(jstk, 1);
  TrigerL = (int)SDL_JoystickGetAxis(jstk, 2);
  AxisRX = (int)SDL_JoystickGetAxis(jstk, 3);
  AxisRY = (int)SDL_JoystickGetAxis(jstk, 4);
  TrigerR = (int)SDL_JoystickGetAxis(jstk, 5);

  return 0;
}


















